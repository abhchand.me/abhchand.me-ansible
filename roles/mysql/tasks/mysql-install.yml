---
- name: Install mysql
  apt:
    name: ['mysql-server', 'mysql-client', 'python2.7-mysqldb']
    state: present
    update_cache: yes

# Allows python to create and manipulate mysql config
- name: Ensure pymysql is present
  pip:
    name: pymysql
    state: present

# #################################################################
# #################################################################

# The below two tasks must both complete together or fail together.
# If only the first passes without the second, subsequent playbook
# runs will fail on the first task.
#
# The issue is outlined in the notes of the `mysql_user` module
# https://docs.ansible.com/ansible/latest/modules/mysql_user_module.html
#
# > MySQL server installs with default login_user of ‘root’ and no
# > password. To secure this user as part of an idempotent playbook,
# > you must create at least two tasks: the first must change the
# > root user’s password, without providing any
# > login_user/login_password details. The second must drop a
# > ~/.my.cnf file containing the new root credentials. Subsequent
# > runs of the playbook will then succeed by reading the new
# > credentials from the file.

- name: Update mysql password for superuser `{{ mysql_superuser }}`
  mysql_user:
    name: "{{ mysql_superuser }}"
    host: localhost
    password: "{{ mysql_superuser_password }}"
    priv: "*.*:ALL,GRANT"

    login_user: root
    login_password: ""
    login_port: "{{ mysql_port }}"
    login_host: localhost
    login_unix_socket: "{{ mysql_socket }}"

    check_implicit_admin: yes

- name: Create system-wide mysql configuration file
  template:
    src: system_wide_mysql.cnf.j2
    dest: /etc/my.cnf

# #################################################################
# #################################################################

- name: Create mysql configuration file for `{{ mysql_superuser }}`
  template:
    src: superuser_mysql.cnf.j2
    dest: "{{ mysql_superuser_home }}/.my.cnf"
  notify:
  - Restart Mysql
